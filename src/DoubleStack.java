import java.util.LinkedList;
import java.util.Stack;
import java.util.regex.Pattern;

public class DoubleStack {
private LinkedList<Double> lList;


   public static void main (String[] argum) {
       System.out.println(interpret("2 x"));
   }

   DoubleStack() {
      this.lList = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
     DoubleStack kloon = new DoubleStack();
      kloon.lList.addAll(lList);
      return kloon;
   }

   public boolean stEmpty() {
      return lList.isEmpty();
   }

   public void push (double a) {
      lList.push(a);
   }

   public double pop() {
       if (stEmpty()) throw new IndexOutOfBoundsException("Puuduvad elemendid");
       return lList.pop();
   }
//Idee pärineb tunnimaterjalist
   public void op (String s) {
       if (stEmpty()) throw new IndexOutOfBoundsException("Puuduvad elemendid, mille vahel aritmeetikatehe teha");
      double arv1 = pop();
      double arv2 = pop();
      if (s.equals("+")) push (arv1 + arv2);
      if (s.equals("-")) push (arv2 - arv1);
      if (s.equals("*")) push (arv1 * arv2);
      if (s.equals("/")) push (arv2 / arv1);

   }
  
   public double tos() {
       if (stEmpty()) throw new IndexOutOfBoundsException("Puudub element, mida lugeda");
       return lList.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      return this.lList.equals(((DoubleStack) o).lList);
   }

   @Override
   public String toString() {
      if (stEmpty())
         return "Tyhi";
      StringBuffer SB = new StringBuffer();
      for (int i = lList.size() - 1; i >= 0; i--)
         SB.append(String.valueOf(lList.get(i)) + " ");
         return SB.toString();

   }
//Idee on pärit: https://codereview.stackexchange.com/questions/120451/reverse-polish-notation-calculator-in-java
   public static double interpret (String pol) {

           Stack<Double> stack = new Stack<>();
           pol = pol.trim();
           for (String token : pol.split("\\s+")) {
               if (!(token.matches("^[0-9]") || token.matches("[ /*+-]")  || token.matches(".*\\p{Punct}"))) {
                   throw new RuntimeException("Võrrand " + pol + " sisaldab tähti");
               }
               else {
                   switch (token) {
                       case "+":
                           stack.push(stack.pop() + stack.pop());
                           break;
                       case "-":
                           stack.push(-stack.pop() + stack.pop());
                           break;
                       case "*":
                           stack.push(stack.pop() * stack.pop());
                           break;
                       case "/":
                           double divisor = stack.pop();
                           stack.push(stack.pop() / divisor);
                           break;

                       default:
                           stack.push(Double.parseDouble(token));
                           break;
                   }
               }
           }
           if (stack.size() > 1) {
               throw new RuntimeException("Avaldis ei ole korrektne, viga võrrandis: " + pol);
           } else {
               return stack.pop();
           }
       }

}

